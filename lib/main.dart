import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        appBar:AppBar(
          backgroundColor: Colors.blueGrey[900],//appBar color
          title: Text(
              "I am Rich"
          ),
        ),
        backgroundColor:Colors.blueGrey[800] ,//scaffold color
        body:Center(
          child: Image(
            image:AssetImage('images/diamond.png')
          ),
        ),
      ),
    ),
  );
}